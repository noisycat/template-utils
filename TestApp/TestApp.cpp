#include <iostream>
#include <ostream>
#include <string>

#include "CommandHandler.h"

void SimpleTestCommand()
{
    std::cout << "I am a simple command! Yey!" << std::endl;
}

void SimpleTestAdd(int a, int b)
{
    std::cout << "I can add! a + b = " << (a + b) << std::endl;
}

struct LittleObject
{
    std::string name;

    void SayMyName() const { std::cout << "Hello, I am " << name << std::endl; }
    void SaySomeNumber(float num) const { std::cout << name << " says " << num << std::endl; }
};

int main()
{
    std::cout << "Hello World!\n";

    //Register some commands
    NDebugCommandTarget testTarget;
    testTarget.RegisterHandler("test", &SimpleTestCommand);
    testTarget.RegisterHandler("add", &SimpleTestAdd, 4);

    //Member methods
    LittleObject thatCould = { "Johnny" };
    LittleObject thatWeMiss = { "RocketMan" };

    testTarget.RegisterHandler("john", &thatCould, &LittleObject::SayMyName);
    testTarget.RegisterHandler("rocket", &thatWeMiss, &LittleObject::SayMyName);
    
    testTarget.RegisterHandler("say", &thatCould, &LittleObject::SaySomeNumber, 5.f);

    //Loop for user input
    std::string input;
    while (std::getline(std::cin, input))
    {
        const NDebugCommand::Result result = testTarget.Execute(input);
        if (result.code != NDebugCommand::CODE_OK)
            std::cout << "Error executing command: \n\t" << result.message << std::endl;
    }
}
