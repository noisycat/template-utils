#include "CommandHandler.h"

#include <ostream>

bool cmp_ignorecase(const std::string& a, const std::string& b)
{
    if (a.length() != b.length()) return false;
    for (int i = 0; i < a.length(); i++)
        if (std::tolower(a[i]) != std::tolower(b[i])) return false;
    return true;
}

NDebugCommand::Result NDebugCommand::ParseString(const std::string& inString,
                                                               std::vector<std::string>& outTokens)
{
    //Mem for single token
    char tok[128];
    uint32_t tokenN = 0;

    outTokens.clear();

    //Parser flags
    bool bQuote = false;
    bool bEscape = false;
    bool bSkipChar = false;
    bool bMakeToken = false;
    
    uint32_t idx = 0;
    const uint32_t len = (uint32_t)inString.length();
    while (idx <= len)
    {
        //Char to write to token
        char c = (idx < len) ? inString[idx++] : 0;
        
        //Eat whitespace between tokens
        if (!bQuote && tokenN == 0 && std::isspace(c))
            continue;

        //Update parser flags if required
        if (bEscape)
        {
            switch (c)
            {
            case '\\': //Escape from escape
            case '"':
                break;
            case 'n':
                c = '\n';
                break;
            case 't':
                c = '\t';
                break;
            case '0':
                c = '\0';
                break;
            case 'x':
            {
                if (idx + 1 >= len)
                    return { CODE_FAIL_PARSE_BAD_HEX, std::string("Not enough hex numbers: `\\") + c + "`!" } ;
                const char hi = inString[idx + 0];
                if (!std::isxdigit(hi))
                    return { CODE_FAIL_PARSE_BAD_HEX, std::string("Invalid HI hex number: `") + hi + "`!" };
                const char lo = inString[idx + 1];
                if (!std::isxdigit(lo))
                    return { CODE_FAIL_PARSE_BAD_HEX, std::string("Invalid LO hex number: `") + lo + "`!" };

                const uint8_t hiVal = (hi >= '0' && hi <= '9') ? (uint8_t)(hi - '0') : (uint8_t)(std::toupper(hi) - 'A' + 10);
                const uint8_t loVal = (lo >= '0' && lo <= '9') ? (uint8_t)(lo - '0') : (uint8_t)(std::toupper(lo) - 'A' + 10);
                c = (char)((hiVal * 0x10) + (loVal * 0x01));
                idx += 2;
                break;
            }
            case 0:  //Command end while escape
                return { CODE_FAIL_PARSE_UNKNOWN_ESC, std::string("Escape sequence not closed!") };
            
            default: //Uknown escape sequence
                return { CODE_FAIL_PARSE_UNKNOWN_ESC, std::string(std::string("Invalid escaped char `\\") + c + "`!") };
            }
            
            bEscape = false;
        }
        else
        {
            switch (c)
            {
            case '"': //Start or finish quote
                if (tokenN > 0)
                {
                    if (idx < len && !std::isspace(inString[idx]))
                        return { CODE_FAIL_PARSE_BAD_QUOTE, std::string("Bad quote `") + c + "`!" };
                    bQuote = false;
                    bMakeToken = true;
                }
                else
                {
                    bQuote = true;
                }
                bSkipChar = true;
                break;
            case ' ':
            case '\t':
            case '\r':
            case '\n':
                if (!bQuote)
                {
                    bSkipChar = true;
                    bMakeToken = true;
                }
                break;
            case '\\':
                bEscape = true;
                bSkipChar = true;
                break;
            case 0: //Finish parsing
                if (bQuote)
                     return { CODE_FAIL_PARSE_BAD_QUOTE, std::string("Quote not closed!") };
                idx++; //Finish outer loop
                bSkipChar = true;
                bMakeToken = true;
                break;
            default:
                break;
            }
        }

        //Resolve
        if (bSkipChar)
            bSkipChar = false;
        else
            tok[tokenN++] = c;

        if (bMakeToken && tokenN > 0)
        {
            tok[tokenN] = '\0';
            outTokens.push_back(std::string(tok));
            tokenN = 0;
        }
        bMakeToken = false;
    }

    return CODE_OK;
}

NDebugCommand::Result NDebugCommandTarget::Execute(const std::string& inCommand)
{
    //Result;
    NDebugCommand::Result result = NDebugCommand::CODE_OK;
    std::vector<std::string> args;
    
    result = NDebugCommand::ParseString(inCommand, args);
    if (result.code != NDebugCommand::CODE_OK)
        return result;

    if (args.size() == 0)
        return { NDebugCommand::CODE_NO_CMD, "No command specified" };

    //Fetch command name, remove it
    const std::string commandName = args[0];
    args.erase(args.begin());
    
    const size_t foundHandler = FindHandlerInternal(commandName);
    if (foundHandler >= handlers.size())
        return { NDebugCommand::CODE_BAD_CMD, "No command named `" + commandName + "` found!" };

    //Finally execute command
    HandlerEntry& targetCmd = handlers[foundHandler];
    result = targetCmd.handler.HandleCommand(args);

    //Resolve error to message if needed
    if (result.code != NDebugCommand::CODE_OK)
    {
        switch (result.code & NDebugCommand::CODE_MASK)
        {
            case NDebugCommand::CODE_BAD_ARGC:
                result.message = "Wrong argument count!";
                break;
            case NDebugCommand::CODE_BAD_ARGV:
                result.message = "Failed to convert argument " + std::to_string(result.code >> 8) + " `" + args[result.code >> 8] = "`";
                break;
            default:
                result.message = "Unknown error.";
                break;
        }
    }
    return result;
}

void NDebugCommandTarget::UnregisterHandler(std::string name)
{
    const size_t idx = FindHandlerInternal(name);
    if (idx < handlers.size())
        handlers.erase(handlers.begin() + idx);
}

size_t NDebugCommandTarget::FindHandlerInternal(std::string name)
{
    for (int i = 0; i < handlers.size(); i++)
        if (cmp_ignorecase(handlers[i].name, name))
            return i;
    return handlers.size();
}
