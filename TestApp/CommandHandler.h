#pragma once
#include <string>
#include <vector>

bool cmp_ignorecase(const std::string& a, const std::string& b);

class NDebugCommand
{
public:
	//Programming API
	NDebugCommand() : Caller(nullptr) { }
	~NDebugCommand() { if (Caller != nullptr) delete Caller; }

	using ECode = uint16_t;
	enum
	{
		CODE_OK = 0,
		CODE_NO_CMD = 1,
		CODE_BAD_CMD = 2,
		CODE_BAD_ARGC = 3,
		CODE_BAD_ARGV = 4, //Second byte is index

		CODE_FAIL_PARSE_UNKNOWN_ESC = 10,
		CODE_FAIL_PARSE_BAD_QUOTE = 11,
		CODE_FAIL_PARSE_BAD_HEX = 12,
		CODE_FAIL_PARSE_NO_CMD = 13,

		CODE_MASK = 0x00FF,
	};

	struct Result {
		Result(ECode inCode, std::string inMessage = "") : code(inCode), message(inMessage) { } 
		ECode code;
		std::string message;
	};

	static Result ParseString(const std::string& inString, std::vector<std::string>& outTokens);

	NDebugCommand(const NDebugCommand&) = delete;
	NDebugCommand& operator=(const NDebugCommand&) = delete;

	NDebugCommand(NDebugCommand&& other) noexcept : Caller(nullptr) { *this = std::move(other); }
	NDebugCommand& operator=(NDebugCommand&& other) noexcept {
		if (Caller) delete Caller;
		Caller = other.Caller;
		other.Caller = nullptr;
		return *this;
	}

	template <typename... TFuncArgs, typename... TFwdArgs>
	explicit NDebugCommand(void(*inFunc)(TFuncArgs...), TFwdArgs... fwdArgs) {
		Caller = new FCaller<void(*)(TFuncArgs...), TFwdArgs...>(inFunc, fwdArgs...);
	}

	template <typename TObj, typename... TFuncArgs, typename... TFwdArgs>
	explicit NDebugCommand(TObj* obj, void(TObj::*inFunc)(TFuncArgs...), TFwdArgs... fwdArgs) {
		Caller = new FCaller<void(TObj::*)(TFuncArgs...), TFwdArgs...>(obj, inFunc, fwdArgs...);
	}
	
	template <typename TObj, typename... TFuncArgs, typename... TFwdArgs>
	explicit NDebugCommand(TObj* obj, void(TObj::*inFunc)(TFuncArgs...)const, TFwdArgs... fwdArgs) {
		Caller = new FCaller<void(TObj::*)(TFuncArgs...)const, TFwdArgs...>(obj, inFunc, fwdArgs...);
	}

	ECode HandleCommand(const std::vector<std::string>& args) const
	{
		if (Caller)
			return Caller->CallFwd(args);
		return false;
	}

private:
	//Our callsite
	struct ICaller { virtual ~ICaller() = default; virtual ECode CallFwd(const std::vector<std::string>& args) = 0; };
	ICaller* Caller;

	template <typename TCaller, typename... TFnArgs> struct FHandler;

	//ARG CONTAINER - contains typed args
	template <typename... TContainerArgs>
	struct FArgContainer
	{
		static constexpr uint32_t TSize = 0;

		template <uint32_t _T>
		// ReSharper disable CppMemberFunctionMayBeStatic - It should match the concept.
		void Get() const {
			static_assert(sizeof(decltype(_T)) == 0, "Default arg match failed!");
		}
		void Unpack() { }
		// ReSharper restore CppMemberFunctionMayBeStatic
	};

	template <typename TSourceType, typename... TRest>
	struct FArgContainer<TSourceType, TRest...>
	{
		TSourceType v;
		FArgContainer<TRest...> d;

		static constexpr uint32_t TSize = sizeof...(TRest) + 1;

		template <uint32_t TIdx, typename TTargetType>
		struct FGetArg {
			using TArg = typename FArgContainer<TRest...>::template FGetArg<TIdx - 1, TTargetType>::TArg;

			template <typename TSubContainer>
			static TTargetType Access(const TSubContainer& Self) { return FArgContainer<TRest...>::template FGetArg<TIdx - 1, TTargetType>::Access(Self.d); }
		};

		template <typename TTargetType>
		struct FGetArg<0, TTargetType> {
			using TArg = TSourceType;

			template <typename TFrom, typename TTarget>
			struct FArgCompatibleCheck {
				static_assert(std::is_convertible_v<TFrom, TTarget>, "Invalid default argument type!");
				static TTarget CheckType(const TFrom& f) { return f; }
			};

			template <typename TSubContainer>
			static TTargetType Access(const TSubContainer& Self) { return FArgCompatibleCheck<TSourceType, TTargetType>::CheckType(Self.v); }
		};

		template <uint32_t TIdx, typename TTargetType>
		TTargetType Get() const {
			return FGetArg<TIdx, TTargetType>::Access(*this);
		}

		void Unpack(TSourceType inV, TRest... rest) {
			v = inV;
			d.Unpack(rest...);
		}
	};

	//CALLER - unify member vs static function pointers
	template <typename... TDefaultArgs>
	struct FCallerBase : ICaller {
		FArgContainer<TDefaultArgs...> defaultArgs;
	};

	template <typename TFunc, typename...>
	struct FCaller { static_assert(sizeof(TFunc) == 0, "Failed to unpack function! FCaller<TFunc>"); };

	template <typename... TFnArgs, typename... TDefaultArgs>
	struct FCaller<void(*)(TFnArgs...), TDefaultArgs...> : FCallerBase<TDefaultArgs...>
	{
		using THandler = FHandler<FCaller<void(*)(TFnArgs...), TDefaultArgs...>, TFnArgs...>;
		FCaller(void (*inFnPtr)(TFnArgs...), TDefaultArgs... args) : fnPtrStatic(inFnPtr) { this->defaultArgs.Unpack(args...); }

		void (*fnPtrStatic)(TFnArgs...); //Store static ptr
		void Fwd(TFnArgs... args) {
			(*fnPtrStatic)(args...);
		}
		ECode CallFwd(const std::vector<std::string>& args) override { return THandler::Call(*this, args, this->defaultArgs); }
	};

	template <typename TOwner, typename... TFnArgs, typename... TDefaultArgs>
	struct FCaller<void(TOwner::*)(TFnArgs...), TDefaultArgs...> : FCallerBase<TDefaultArgs...>
	{
		using THandler = FHandler<FCaller<void(TOwner::*)(TFnArgs...), TDefaultArgs...>, TFnArgs...>;
		FCaller(TOwner* inOwner, void(TOwner::* inFnPtr)(TFnArgs...), TDefaultArgs... args) : owner(inOwner), fnPtrMember(inFnPtr) { this->defaultArgs.Unpack(args...); }

		TOwner* owner;							  //Store object ptr
		void (TOwner::* fnPtrMember)(TFnArgs...); //Store member ptr
		void Fwd(TFnArgs... args) {
			(owner->*fnPtrMember)(args...);
		}
		ECode CallFwd(const std::vector<std::string>& args) override { return THandler::Call(*this, args, this->defaultArgs); }
	};

	template <typename TOwner, typename... TFnArgs, typename... TDefaultArgs>
    struct FCaller<void(TOwner::*)(TFnArgs...)const, TDefaultArgs...> : FCallerBase<TDefaultArgs...>
	{
		using THandler = FHandler<FCaller<void(TOwner::*)(TFnArgs...)const, TDefaultArgs...>, TFnArgs...>;
		FCaller(TOwner* inOwner, void(TOwner::* inFnPtr)(TFnArgs...)const, TDefaultArgs... args) : owner(inOwner), fnPtrMember(inFnPtr) { this->defaultArgs.Unpack(args...); }

		TOwner* owner;							  	   //Store object ptr
		void (TOwner::* fnPtrMember)(TFnArgs...)const; //Store member ptr
		void Fwd(TFnArgs... args) {
			(owner->*fnPtrMember)(args...);
		}
		ECode CallFwd(const std::vector<std::string>& args) override { return THandler::Call(*this, args, this->defaultArgs); }
	};

	//HANDLER - create generic dispatcher from vec<string> to function call
	template <typename TCaller, typename... TFnArgs>
	struct FHandler
	{
		static constexpr uint32_t TFnArgC = sizeof...(TFnArgs);

		//Convert - use static match to convert argument from string to given type
		template <typename TConvertTo, typename = void>
		struct FConvert { static_assert(sizeof(TConvertTo) == 0, "Cannot convert! FConvert<TConvertTo>"); };

		template <typename _T>
		struct FConvert<std::string, _T> { static std::string Do(const std::string& inStr) { return inStr; } };

		template <typename _T>
		struct FConvert<int32_t, _T> { static int32_t Do(const std::string& inStr) { return std::stoi(inStr); } };

		template <typename _T>
		struct FConvert<float, _T> { static float Do(const std::string& inStr) { return std::stof(inStr); } };

		template <typename _T>
		struct FConvert<bool, _T> { static int32_t Do(const std::string& inStr) { return (inStr != "0") && !cmp_ignorecase(inStr, "false") && !cmp_ignorecase(inStr, "no"); } };

		//Unpack - move args one by one from FUnpacker<> to Call<> template
		template <uint32_t, typename TArgContainer, typename...> //Unpack index and what is left to unpack
		struct FUnpacker {
			template <typename... TFwdArgs> //Unpacked (converted) arguments are aggregated here
			static ECode Call(TCaller& fn, const std::vector<std::string>&, const TArgContainer&, TFwdArgs... fwdArg)
			{
				//We have no more args to unpack, finally call
				fn.Fwd(fwdArg...);
				return CODE_OK;
			}
		};

		//Specialize for at least one argument to unpack
		template<uint32_t TIndex, typename TArgContainer, typename TFirst, typename... TUnpArgs>
		struct FUnpacker<TIndex, TArgContainer, TFirst, TUnpArgs...>
		{
			enum class EArgumentAccess : uint8_t {
				Arg, ArgChecked, ArgOrDefault
			};

			//Forward to next FUnpacker, use default if specified
			template <EArgumentAccess TAccess, typename... TFwdArgs>
			struct FAccessArgument {
				static ECode FwdCall(TCaller& fn, const std::vector<std::string>& unpArg, const TArgContainer& defaultArgs, TFwdArgs... fwdArg) {
					try
					{
						return FUnpacker<TIndex + 1, TArgContainer, TUnpArgs...>::
							template Call<TFwdArgs..., TFirst>(fn, unpArg, defaultArgs, fwdArg..., FConvert<TFirst>::Do(unpArg[TIndex]));
					}
					catch (...) {}
					return CODE_BAD_ARGV | (TIndex << 8);
				}
			};

			template <typename... TFwdArgs>
			struct FAccessArgument<EArgumentAccess::ArgChecked, TFwdArgs...> {
				static ECode FwdCall(TCaller& fn, const std::vector<std::string>& unpArg, const TArgContainer& defaultArgs, TFwdArgs... fwdArg) {
					if (TIndex < unpArg.size()) //If available use arg
						return FAccessArgument<EArgumentAccess::Arg, TFwdArgs...>::FwdCall(fn, unpArg, defaultArgs, fwdArg...);
					return CODE_BAD_ARGC; //Fail with bad arg
				}
			};

			template <typename... TFwdArgs>
			struct FAccessArgument<EArgumentAccess::ArgOrDefault, TFwdArgs...> {
				static ECode FwdCall(TCaller& fn, const std::vector<std::string>& unpArg, const TArgContainer& defaultArgs, TFwdArgs... fwdArg) {
					if (TIndex < unpArg.size()) //If provided use arg
						return FAccessArgument<EArgumentAccess::Arg, TFwdArgs...>::FwdCall(fn, unpArg, defaultArgs, fwdArg...);
					return FUnpacker<TIndex + 1, TArgContainer, TUnpArgs...>:: //Fallback to default
						template Call<TFwdArgs..., TFirst>(fn, unpArg, defaultArgs, fwdArg..., defaultArgs.template Get<TArgContainer::TSize + TIndex - TFnArgC, TFirst>());
				}
			};

			//Actually unpack the argument, pass to next unpacker
			template <typename... TFwdArgs>
			static ECode Call(TCaller& fn, const std::vector<std::string>& unpArg, const TArgContainer& defaultArgs, TFwdArgs... fwdArg)
			{
				//Consume TFirst from FUnpacker<>, pass to FUnpacker::Call<>
				return FAccessArgument<(TIndex >= TFnArgC - TArgContainer::TSize) ? EArgumentAccess::ArgOrDefault : EArgumentAccess::ArgChecked, TFwdArgs...>::
					FwdCall(fn, unpArg, defaultArgs, fwdArg...);
			}
		};

		//Forward to unpacker, entry point for unpack/call
		template <typename TArgContainer>
		static ECode Call(TCaller& fn, const std::vector<std::string>& args, const TArgContainer& defaultArgs) {
			return FUnpacker<0, TArgContainer, TFnArgs...>::Call(fn, args, defaultArgs);
		}
	};
};

class NDebugCommandTarget
{
public:
	NDebugCommand::Result Execute(const std::string& inCommand);

	template <typename... TFwd>
    void RegisterHandlerXX(std::string name, TFwd... fws) { handlers.emplace_back(name, NDebugCommand(fws...)); };

	template <typename... TFuncArgs, typename... TFwdArgs>
	void RegisterHandler(std::string name, void(*inFunc)(TFuncArgs...), TFwdArgs... fwdArgs) {
		handlers.emplace_back(name, NDebugCommand(inFunc, fwdArgs...));
	}
	template <typename TObj, typename... TFuncArgs, typename... TFwdArgs>
    void RegisterHandler(std::string name, TObj* obj, void(TObj::* inFunc)(TFuncArgs...), TFwdArgs... fwdArgs) {
		handlers.emplace_back(name, NDebugCommand(obj, inFunc, fwdArgs...));
	}
	template <typename TObj, typename... TFuncArgs, typename... TFwdArgs>
    void RegisterHandler(std::string name, TObj* obj, void(TObj::* inFunc)(TFuncArgs...)const, TFwdArgs... fwdArgs) {
		handlers.emplace_back(name, NDebugCommand(obj, inFunc, fwdArgs...));
	}
	void UnregisterHandler(std::string name);
    
private:
	size_t FindHandlerInternal(std::string name);
    
	struct HandlerEntry
	{
		HandlerEntry(std::string inName, NDebugCommand&& inHandler)
            : name(inName), handler(std::move(inHandler)) { }
        
		std::string name;
		NDebugCommand handler;
	};

	std::vector<HandlerEntry> handlers;
};
